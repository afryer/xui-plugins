Video plugin for XUI
Version:0.01


Exposes some simple functions for custom controls of HTML5 video. Aim is mobile focused and lightweight.

Currently only focusing on mp4 with flash fallback as this gives best user coverage on mobile. 



ROADMAP: 
Playlists working better
sort out passing the video to the functions
timer


SOME EXAMPLES
    
Video Detection, this will be expanded on and is only a starting point.      
    x$('#video').videoDetection();

Creates a draggable progress bar
    x$('#progress').progressBar(); 

this is just straight out html5       
    x$( '#play' ).on( 'click', function(e) {
        var video = document.getElementsByTagName('video')[0];
        video.play();
    });

This is straight out HTML5
    x$( '#pause' ).on( 'click', function(e) {
        var video = document.getElementsByTagName('video')[0];
        video.pause();
    });

toggle play or pause
    x$('#playPause').on('click',function(e){
        var video = document.getElementsByTagName('video')[0];
        x$(this).playOrPause(video);
    });
        
Toggle mute - broken at the moment
    x$('#mutebutton').on('click',function(e){
        var video = document.getElementsByTagName('video')[0];
        x$(this).muteButton(video);
    });
    
Create a playlist - needs work
    x$('#playlist a').on('click',function(event){
        _this = x$(this).attr('href');
       var video = document.getElementsByTagName('video')[0];
  var sources = video.getElementsByTagName('source');
console.log(sources);
console.log(_this);
        
        event.preventDefault();
        var video = document.getElementsByTagName('video')[0];
        x$(this).playlist(video);

    });

        
});


