/**********************************************************************    
*
*Video Plugin for XUI
*Author: Anthony Fryer
*
*This program is free software: you can redistribute it and/or modify it 
*under the terms of the GNU General Public License as published by the 
*Free Software Foundation, either version 3 of the License, or any later version. 
*
*This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
*without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
*See the GNU General Public License for more details. 
*
*insipration and a lot of code from:
*http://www.slideshare.net/gregthebusker/mobile-meow-at-mobilism-12961772
*
*
***********************************************************************/
 function showIt(elID)
    {
      var el = document.getElementById(elID);
      el.scrollIntoView(true);
    }
    
xui.extend ({
    playOrPause:function(video) {
        var playPause = x$(this)[0];     
        //play or pause         
        video.onpause = video.onplay = function(e) {
        playPause.value = video.paused ? 'Play' : 'Pause';
}
        if (video.ended || video.paused) {
            playPause.setAttribute('class', 'pause');
            playPause.value = 'Pause';

            video.play();
        } else {
            playPause.setAttribute('class', 'play');
            playPause.value = 'Play';
            video.pause();
        }
  


    },
    
// needs work, was working?
    muteButton : function(video){
        var mutebutton = x$(this)[0];
        console.log(mutebutton);
        video.onvolumechange = function(e) {
            mutebutton.value = video.muted ? 'Muted' : 'Unmuted';
        }
        function muteOrUnmute() {
                video.muted = !video.muted;
        }
    },


    
  progressBar : function(video){
      
        var dragging = false;
        var video = document.getElementsByTagName('video')[0];
        var playheadElement = document.getElementById('playhead');
        var progressElement = document.getElementById('progress');
      
      
        // Bind handlers.
        video.addEventListener('timeupdate', updatePlayhead);
        playheadElement.onmousedown = playheadElement.ontouchstart = startDrag;
  

        function updatePlayhead() {
            if (!dragging) {
                playheadElement.style.left = (video.currentTime / video.duration) * 100 + '%';
            }
        }


        var dragPosition;
        var dragOffset;
        function startDrag(evt) {
            dragging = true;
            // May look familiar to those that read 
            // http://www.quirksmode.org/blog/archives/2010/02/the_touch_actio.html
            if (evt.type === 'touchstart') {
                playheadElement.onmousedown = null;
                playheadElement.ontouchmove = drag;
                playheadElement.ontouchend = stopDrag;
            } else {
                document.onmousemove = drag;
                document.onmouseup = stopDrag;
            }
            dragOffset = playheadElement.offsetLeft;
            dragPosition = getClientX(evt);
        }


        function drag(evt) {

            // Figure out the progress fraction.
            var position = getClientX(evt);
            var offset = dragOffset + (position - dragPosition);
            var fraction = offset / progressElement.getClientRects()[0].width;
            fraction = Math.max(0, Math.min(1, fraction));

            // Adjust UI and notify video of a seek.
            playheadElement.style.left = fraction * 100 + '%';
            video.currentTime = video.duration * fraction;

            // Prevent page scrolling
            evt.preventDefault();
            return false;
        }
      
      
        function stopDrag() {
            dragging = false;
            document.onmousemove = null;
            document.onmouseup = null;
            playheadElement.ontouchmove = null;
            playheadElement.ontouchend = null;
        }
      
      
        function getClientX(evt) {
            if (evt.touches && evt.touches.length) { 
                return evt.touches[0].clientX;
            }
            return evt.clientX;
        }
},



      playlist : function(thevid) {
x$(this).on('click',function(e){
         showIt('video');
         var newVid = x$(this).attr('href');
         var newPoster = x$(this).attr('data-image');	
         var newTitle = x$(this).attr('data-header').toString();
            var newText = x$(this).attr('data-text').toString();

        x$(thevid).attr('src', newVid);
        x$(thevid).attr('poster', newPoster);
        x$('h2#videoTitle').html(newTitle);
        x$('p#videoText').html(newText);

            if (video.ended || video.paused) {
            playPause.setAttribute('class', 'pause');
            playPause.value = 'play';
            // think this is breaking on bb no autostart? changed playpause value to reflect 
            // video.play();
            }
        return false;
});
},
    videoDetection:function(){
        //  Start of decent video detection
        //  
        //  Modernizer or roll own? 
        //
        //
        var video = document.getElementsByTagName('video')[0];
        if (video.canPlayType) { // <video> is supported!
            //this and android 2.1 is a bit knackered
            if (video.canPlayType('video/mp4')) {         
            $('#videoError').addClass('hidden');
            // it can play (maybe)!
            } else {
                // the container format or codecs aren't supported
                // let's fall back
                fallback(video);
            }
        }
        function fallback(video) {
            // this is dodgy on the orange san fransisco?? 2.1
            alert ('fallback alert');
}
    }
});