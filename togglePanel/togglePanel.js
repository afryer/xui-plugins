/* for toggling a panel using emile and xui
 * 
 * example 
 * on load hide the div by adding a class with height:0px - for those non js browsers / users
 * then on click (or any other xui option
 * 
 *  x$(window).load(function(e){
 *     x$('#item').addClass('closed');    
 *       x$('.panel a.panelItem').click(function(e){
 *        x$('#item').togglePanel('slow','item');})
*       });
 * 
 * @TODO add easing option and remove the need to add the item twice
 */
xui.extend (
    {
        
     togglePanel:function(dur,thePanel)
     {
          if (dur === "slow"){
             dur = 1500;
         }
         else if (dur === "fast"){
             dur = 500;
         }
       
         
        var panel = document.getElementById(thePanel);
        var theHeight =  document.getElementById(thePanel).scrollHeight;
            if(x$(this).hasClass('closed')){
            emile(panel, 'height:'+theHeight+'px',{duration:dur,easing:bounce, after: function() {
           x$(panel).removeClass('closed');
}});

            }            
            else {
            emile(panel, 'height:0px', {duration:dur,easing:easeInStrong, after: function() {
            x$(panel).addClass('closed');    
}});
            }
             
            
        }
    });
    
/*easing functions taken from https://github.com/ded/emile
 * No longer maintained by ded so taking stuff while I can - Cheers!!
 * 
 */    
    function easeOut (pos) {
    return Math.sin(pos * Math.PI / 2);
  };

  function easeOutStrong (pos) {
    return (pos == 1) ? 1 : 1 - Math.pow(2, -10 * pos);
  };

  function easeIn (pos) {
    return pos * pos;
  };

   function easeInStrong(pos) {
    return (pos == 0) ? 0 : Math.pow(2, 10 * (pos - 1));
  };

                function bounce(pos) {
    if (pos < (1/2.75)) {
        return (7.5625*pos*pos);
    } else if (pos < (2/2.75)) {
        return (7.5625*(pos-=(1.5/2.75))*pos + .75);
    } else if (pos < (2.5/2.75)) {
        return (7.5625*(pos-=(2.25/2.75))*pos + .9375);
    } else {
        return (7.5625*(pos-=(2.625/2.75))*pos + .984375);
    }
  };